<?php


namespace Eiprice\Utils;

/**
 * Class Data
 * @package Eiprice\Utils
 */
class Data
{
    /**
     *
     * @param $data
     * @return string
     */
    public static function hash($data) : string
    {
        return is_string($data) ? md5($data) : md5( json_encode($data) );
    }

    public static function pre_print_r($data) : string
    {
        return '<pre>' . json_encode($data) . '</pre>';
    }


    public static function blade_preview($data) : string
    {
        return is_string($data) || is_numeric($data) ? $data : self::pre_print_r($data);
    }
}
