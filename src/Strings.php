<?php


namespace Eiprice\Utils;

/**
 * Class String
 * @package Eiprice\Utils
 */
class Strings
{
    /**
     *
     * @param $string
     * @return bool
     */
    public static function isURL($string) : bool
    {
        return is_string($string) ?
            (bool) preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$string)
            : false;
    }

    /**
     * @param mixed ...$list
     * @return string
     */
    public static function concat(... $list) : string
    {
        return implode('', $list);
    }


    /**
     * @param $string
     * @return string
     */
    public static function removerAcento(string $string) : string
    {
        return preg_replace(
            array(
                '/(á|à|ã|â|ä)/',
                '/(Á|À|Ã|Â|Ä)/',
                '/(é|è|ê|ë)/',
                '/(É|È|Ê|Ë)/',
                '/(í|ì|î|ï)/',
                '/(Í|Ì|Î|Ï)/',
                '/(ó|ò|õ|ô|ö)/',
                '/(Ó|Ò|Õ|Ô|Ö)/',
                '/(ú|ù|û|ü)/',
                '/(Ú|Ù|Û|Ü)/',
                '/(ñ)/',
                '/(Ñ)/'
            ),
            explode(' ','a A e E i I o O u U n N'),
            $string
        );
    }
}
